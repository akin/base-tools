#include <base/local.h>
#include <base/singleton.h>
#include "localfile.h"
#include "stdlog.h"

namespace Base {
namespace Local {

Interface::File::Shared open(const std::string& path)
{
    return Interface::File::Shared(new LocalFile{path});
}

Interface::File::Info info(const std::string& path)
{
    Interface::File::Info info;

    return info;
}

Interface::Log& getLog()
{
    return getSingleton<StdLog>();
}

#if defined(OS_MAC)
// https://raw.githubusercontent.com/NickStrupat/CacheLineSize/master/CacheLineSize.c
// http://stackoverflow.com/questions/39680206/understanding-stdhardware-destructive-interference-size-and-stdhardware-cons
// http://stackoverflow.com/questions/794632/programmatically-get-the-cache-line-size

size_t getCacheLineSize() {
    size_t lineSize = 0;
    size_t sizeOfLineSize = sizeof(lineSize);
    sysctlbyname("hw.cachelinesize", &lineSize, &sizeOfLineSize, 0, 0);
    return lineSize;
}

#elif defined(OS_WINDOWS)

size_t getCacheLineSize() {
	size_t lineSize = 0;
	DWORD bufferSize = 0;
	DWORD i = 0;
	SYSTEM_LOGICAL_PROCESSOR_INFORMATION * buffer = 0;

	GetLogicalProcessorInformation(0, &bufferSize);
	buffer = (SYSTEM_LOGICAL_PROCESSOR_INFORMATION *) malloc(bufferSize);
	GetLogicalProcessorInformation(&buffer[0], &bufferSize);

	for (i = 0; i != bufferSize / sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION); ++i) {
		if (buffer[i].Relationship == RelationCache && buffer[i].Cache.Level == 1) {
			lineSize = buffer[i].Cache.LineSize;
			break;
		}
	}

	free(buffer);
	return lineSize;
}

#elif defined(OS_LINUX)
size_t getCacheLineSize() {
	FILE * p = 0;
	p = fopen("/sys/devices/system/cpu/cpu0/cache/index0/coherency_line_size", "r");
	unsigned int lineSize = 0;
	if (p) {
		fscanf(p, "%d", &lineSize);
		fclose(p);
	}
	return lineSize;
}
#else
size_t getCacheLineSize() {
    return 64;
}
#endif

} // ns Local
} // ns Base

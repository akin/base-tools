
#ifndef BASE_LOCALFILE_H_
#define BASE_LOCALFILE_H_

#include <interface/file.h>
#include <string>
#include <fstream>

namespace Base {

class LocalFile : public Interface::File
{
private:
    std::fstream m_stream;
    File::Info m_info;
public:
    LocalFile(const std::string& path);
    ~LocalFile();

    bool isOpen() const override;
    Interface::File::Info info() const override;

    bool open(uint8_t access = Access::READ | Access::WRITE | Access::APPEND) override;
    void reset() override;
    void close() override;
    void flush() override;

    void seek(size_t position) override;
    size_t get() override;
    
    size_t read(size_t amount, uint8_t *buffer) override;
    size_t write(size_t size, const uint8_t *buffer) override;

    size_t read(size_t offset, size_t amount, uint8_t *buffer) override;
    size_t write(size_t offset, size_t size, const uint8_t *buffer) override;
};

} // ns Base

#endif // BASE_LOCALFILE_H_

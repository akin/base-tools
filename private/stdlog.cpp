
#include "stdlog.h"
#include "localutil.h"
#include <fmt/format.h>
#include <iostream>

namespace Base {

StdLog::~StdLog()
{
}

void StdLog::print(const char *file, size_t line, Level level, std::string message)
{
	std::string out = fmt::format("{2} {0}:{1} - {3}\n", file, line, toString(level), message);
#ifdef PLATFORM_WINDOWS
	OutputDebugString(out.c_str());
#endif
	std::cout << out;
}

void StdLog::print(Level level, std::string message)
{
	std::string out = fmt::format("{0} : {1}\n", toString(level), message);
#ifdef PLATFORM_WINDOWS
	OutputDebugString(out.c_str());
#endif
	std::cout << out;
}

} // ns Base


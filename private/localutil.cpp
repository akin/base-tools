
#include "localutil.h"

namespace Base {

std::string toString(Interface::Log::Level level)
{
	if (level < Interface::Log::Level::Warning)
	{
		return "Log";
	}
	if (level < Interface::Log::Level::Error)
	{
		return "Warning";
	}
	return "Error";
}

} // ns Base
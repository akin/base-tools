
#ifndef BASE_LOCALUTIL_H_
#define BASE_LOCALUTIL_H_

#include <interface/log.h>
#include <string>

namespace Base {

std::string toString(Interface::Log::Level level);

} // ns Base

#endif // BASE_LOCALUTIL_H_


#ifndef BASE_STD_LOG_H_
#define BASE_STD_LOG_H_

#include <interface/log.h>

namespace Base {

class StdLog : public Interface::Log 
{
public:
    ~StdLog() override;

	void print(const char *file, size_t line, Level level, std::string message) override;
	void print(Level level, std::string message) override;
};

} // ns Base

#endif // BASE_STD_LOG_H_

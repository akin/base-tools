cmake_minimum_required(VERSION 3.7)
project(base-tools C CXX)
add_library(${PROJECT_NAME} STATIC)

set(CMAKE_MODULE_PATH 
	${CMAKE_MODULE_PATH} 
	"${CMAKE_CURRENT_SOURCE_DIR}/cmake"
)

file(GLOB CURRENT_SOURCES 
	${CMAKE_CURRENT_LIST_DIR}/private/*.cpp 
	${CMAKE_CURRENT_LIST_DIR}/private/*.c
	${CMAKE_CURRENT_LIST_DIR}/private/*.inl
	${CMAKE_CURRENT_LIST_DIR}/private/*.h
	${CMAKE_CURRENT_LIST_DIR}/public/base/*.inl
	${CMAKE_CURRENT_LIST_DIR}/public/base/*.h
)

target_sources(
	${PROJECT_NAME} 
	PRIVATE 
		${CURRENT_SOURCES}
	)

target_include_directories(
	${PROJECT_NAME} 
	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/private"
	PUBLIC 
		"${CMAKE_CURRENT_LIST_DIR}/public"
)

target_link_libraries(
    ${PROJECT_NAME} 
	PUBLIC
    	base
		fmt-header-only
)

add_subdirectory("test")
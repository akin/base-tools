
#ifndef BASE_TOOLS_LOCAL_TEST_H_
#define BASE_TOOLS_LOCAL_TEST_H_

#include <iostream>
#include "catch.hpp"

#include <base/local.h>

TEST_CASE( "Simple base-tools tests local.", "[]" ) {


    // Create data
    // Write data
    // Load data
    // Verify data

    const size_t max = 10000;
    std::vector<uint8_t> original;
    original.reserve(max);

    for(size_t i = 0 ; i < max ; ++i)
    {
        original.push_back(i % 0xFF);
    }

    std::string testPath = "data.bin";
    {
        auto file = Base::Local::open(testPath);
        REQUIRE(file->open(Base::WRITE));

        size_t count = file->write(original.size(), original.data());

        REQUIRE(count == original.size());
    }

    SECTION( "Load test.data and test it" ) {
        std::vector<uint8_t> copied;

        auto file = Base::Local::open(testPath);
        REQUIRE(file->open(Base::READ));

        size_t foundSize = file->info().size;
        copied.resize(foundSize);
        size_t count = file->read(foundSize, copied.data());
        REQUIRE(count == foundSize);

        REQUIRE(foundSize == max);

        for(size_t i = 0 ; i < foundSize ; ++i)
        {
            REQUIRE(original[i] == copied[i]);
        }
    }
}

#endif // BASE_TOOLS_LOCAL_TEST_H_

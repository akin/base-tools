
#ifndef BASE_TOOLS_TASKMANAGER_H_
#define BASE_TOOLS_TASKMANAGER_H_

#include <iostream>
#include <thread>
#include <functional>
#include <interface/manager.h>
#include <interface/task.h>
#include <interface/taskmanager.h>

#include <thread/queue.h>
#include <thread/observable.h>

namespace Base {

class TaskManager : public Interface::Manager, public Interface::TaskManager
{
private:
    Thread::Queue<Interface::Task::Shared> m_queue;
    std::atomic_bool m_alive;
    std::atomic<size_t> m_running;
    mutable std::mutex m_mutex;
    std::condition_variable m_condition;
public:
    TaskManager();
    virtual ~TaskManager();

    void run() override;
    void close() override;
    
    void clear() override;
    void add(Interface::Task::Shared work) override;
};

} // ns Base

#endif // BASE_TOOLS_TASKMANAGER_H_

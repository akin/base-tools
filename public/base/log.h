

#ifndef BASE_LOG_LOCAL_H_
#define BASE_LOG_LOCAL_H_

#include "local.h"
#include <fmt/format.h>

 // LOG Uses fmt style formatting
 // https://github.com/fmtlib/fmt
 // LOG_MESSAGE("Free the {}", "whales");
 // LOG_MESSAGE("Free the {0} {1}, {0}", "whales", "now");
#define LOG_MESSAGE(msg, ...) ::Base::Local::getLog().print(__FILE__, __LINE__, ::Interface::Log::Level::Message, fmt::format(msg,  __VA_ARGS__))
#define LOG_WARNING(msg, ...) ::Base::Local::getLog().print(__FILE__, __LINE__, ::Interface::Log::Level::Warning, fmt::format(msg,  __VA_ARGS__))
#define LOG_ERROR(msg, ...) ::Base::Local::getLog().print(__FILE__, __LINE__, ::Interface::Log::Level::Error, fmt::format(msg,  __VA_ARGS__))

#endif // BASE_LOG_LOCAL_H_





#ifndef BASE_TOOLS_LOCAL_H_
#define BASE_TOOLS_LOCAL_H_

#include <string>
#include <memory>
#include "osheaders.h"

#include <interface/file.h>
#include <interface/log.h>

namespace Base {
namespace Local {

Interface::File::Shared open(const std::string& path);
Interface::File::Info info(const std::string& path);

Interface::Log& getLog();

size_t getCacheLineSize();

} // ns Local
} // ns Base

#endif // BASE_TOOLS_LOCAL_H_

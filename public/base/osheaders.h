
#ifndef OS_HEADERS_INC_
#define OS_HEADERS_INC_

#include "os.h"

#if defined(OS_MAC)
# include <sys/sysctl.h>
#elif defined(OS_WINDOWS)
# include <stdlib.h>
# include <windows.h>
#elif defined(OS_LINUX)
# include <stdio.h>
#endif

#endif